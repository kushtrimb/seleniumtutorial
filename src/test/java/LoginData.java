/**
 * Created by Kushtrim.Bytyqi on 9/6/2017.
 */
public class LoginData {

    private String username;
    private String password;
    private String name;

    public LoginData(String un, String ps, String name)
    {
        username = un;
        password = ps;
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }
}
