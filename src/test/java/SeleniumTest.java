import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by Kushtrim.Bytyqi on 9/6/2017.
 */
public class SeleniumTest {

    Settings settings;
    LoginPage login;
    NewCompanyPage newCompanyPage;
    LoginData data;
    NewCompanyData newCompanyData;

    @BeforeTest
    public void init()
    {
        login = new LoginPage();
        newCompanyPage = new NewCompanyPage();
        data = new LoginData("tradchenko@bottomline.com", "password", "Tetiana");
        newCompanyData = new NewCompanyData("Test1","Shpat Braina", "shpat.braina@bottomline.com","1000","2000","3000");
        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        settings = Settings.getInstance();
        settings.setDriver(driver);
        driver.get("https://ptxadm-dev-app02.bts.local/ui/brand");
        PageFactory.initElements(driver, login);
        PageFactory.initElements(driver, newCompanyPage);
    }

    @Test
    public void test() throws InterruptedException
    {
        login.login(data.getUsername(), data.getPassword());
        Thread.sleep(10000);
        newCompanyPage.register( newCompanyData.getCompanyName(), newCompanyData.getContactName(), newCompanyData.getContactEmail(), newCompanyData.getAcode(), newCompanyData.getScode(), newCompanyData.getCcode());
        //org.testng.Assert.assertEquals(data.getName(),login.getUserName());
    }
}