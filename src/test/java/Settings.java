import org.openqa.selenium.WebDriver;

public class Settings {

    private static Settings instance = null;
    private WebDriver driver;

    protected Settings(){

    }

    public static Settings getInstance(){
        if(instance == null){
            instance = new Settings();
        }
        return instance;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
}
