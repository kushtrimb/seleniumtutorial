import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Kushtrim.Bytyqi on 9/6/2017.
 */

public class LoginPage
{
    @FindBy(id = "username")
    WebElement username;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(id = "login-btn")
    WebElement loginButton;

    @FindBy(css = ".pull-left")
    WebElement labelUsername;

    private void setUsername(String username)
    {
        this.username.sendKeys(username);
    }

    private void setPassword(String password)
    {
        this.password.sendKeys(password);
    }

    private void clickLogin()
    {
        loginButton.click();
    }

    public String getUserName()
    {
        return labelUsername.getText();
    }

    public void login(String userName, String password)
    {
        setUsername(userName);
        setPassword(password);
        clickLogin();
    }
}
