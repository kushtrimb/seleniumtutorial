import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewCompanyPage {

    private WebDriver driver;

    @FindBy(xpath = "//a[@href='/ui/brand/companies/create']")
    private WebElement newCompanyBttn;

    @FindBy(css = "[type='submit']")
    private WebElement saveCompanyBttn;

    @FindBy(id = "name")
    private WebElement companyName;

    @FindBy(id = "contactName")
    private WebElement contactName;

    @FindBy(id = "contactEmail")
    private WebElement contactEmail;

    @FindBy(id = "acode")
    private WebElement acode;

    @FindBy(id = "scode")
    private WebElement scode;

    @FindBy(id = "ccode")
    private WebElement ccode;

    public void register(String companyNameData, String contactNameData, String contactEmailData, String acodeData, String scodeData, String ccodeData) throws InterruptedException {
        driver = Settings.getInstance().getDriver();
        newCompanyBttn.click();
        Thread.sleep(5000);
        companyName.sendKeys(companyNameData);
        contactName.sendKeys(contactNameData);
        contactEmail.sendKeys(contactEmailData);
        acode.sendKeys(acodeData);
        scode.sendKeys(scodeData);
        ccode.sendKeys(ccodeData);
        Thread.sleep(10000);
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        saveCompanyBttn.click();
    }

    public WebElement getNewCompanyBttn() {

        return newCompanyBttn;
    }

    public void setNewCompanyBttn(WebElement newCompanyBttn) {
        this.newCompanyBttn = newCompanyBttn;
    }

    public WebElement getCompanyName() {
        return companyName;
    }

    public void setCompanyName(WebElement companyName) {
        this.companyName = companyName;
    }

    public WebElement getSaveCompanyBttn() {
        return saveCompanyBttn;
    }

    public void setSaveCompanyBttn(WebElement saveCompanyBttn) {
        this.saveCompanyBttn = saveCompanyBttn;
    }

    public WebElement getContactName() {
        return contactName;
    }

    public void setContactName(WebElement contactName) {
        this.contactName = contactName;
    }

    public WebElement getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(WebElement contactEmail) {
        this.contactEmail = contactEmail;
    }

    public WebElement getAcode() {
        return acode;
    }

    public void setAcode(WebElement acode) {
        this.acode = acode;
    }

    public WebElement getScode() {
        return scode;
    }

    public void setScode(WebElement scode) {
        this.scode = scode;
    }

    public WebElement getCcode() {
        return ccode;
    }

    public void setCcode(WebElement ccode) {
        this.ccode = ccode;
    }
}
